# Coverage
[![Build status](https://gitlab.com/spinit/opensymap/badges/master/build.svg)](https://gitlab.com/spinit/opensymap/commits/master)
[![Overall test coverage](https://gitlab.com/spinit/opensymap/badges/master/coverage.svg)](https://spinit.gitlab.io/opensymap/)

# Opensymap
opensymap is a library for fast development of management software.