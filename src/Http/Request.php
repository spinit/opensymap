<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Http;

use Respect\Rest\Request as RestRequest;

use Spinit\Util;

/**
 * Description of Request
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Request extends RestRequest
{
    public $domain = '';
    public $body = '';
    public $headers = array();
    public function __construct($method = null, $domain = null, $uri = null, $body = '', $headers = array())
    {
        //$_SERVER['REQUEST_METHOD'] = Util\nvl($method, Util\arrayGet($_SERVER, 'REQUEST_METHOD'));
        //$_SERVER['REQUEST_URI']    = Util\nvl($uri, Util\arrayGet($_SERVER, 'REQUEST_URI'));
        parent::__construct($method, $uri);
        $this->domain = $domain;
        $this->body = $body;
        $this->headers = $headers;
    }
}
