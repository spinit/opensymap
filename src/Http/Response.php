<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Http;

use Spinit\Util;

/**
 * Description of Response
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Response
{
    protected $content = '';
    protected $headers = array();
    public function __construct($content = '')
    {
        $this->content = $content;
    }
    public function header($header)
    {
        $this->headers[] = $header;
    }
    public function set($key, $value)
    {
        if (!is_array($this->content)) {
            $this->content = array();
        }
        $this->content[$key] = $value;
    }
    public function get($key)
    {
        if (!is_string($this->content)) {
            return Util\arrayGet($this->content, $key, null);
        }
        return null;
    }
    protected function serializeHeaders()
    {
        foreach ($this->headers as $header) {
            header($header);
        }
    }
    protected function serializeContent()
    {
        if (!is_string($this->content)) {
            return \json_encode($this->content);
        }
        return $this->content;
    }
    public function __toString()
    {
        $this->serializeHeaders();
        return $this->serializeContent();
    }
}
