<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Instance;

use Spinit\Opensymap\Core\Instance;
use Spinit\Opensymap\Implementation\Xml\ApplicationXml;

/**
 * Description of Item
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Install extends Instance
{
    public function getApplication()
    {
        $root = implode(DIRECTORY_SEPARATOR, array(dirname(dirname(__DIR__)), 'app', 'Install'));
        $app = new ApplicationXml($this, $root, 'Core:Main');
        return $app;
    }
}
