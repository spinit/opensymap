<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Core;

use Spinit\Opensymap\Core\Application;
use Spinit\Opensymap\Lib\Dictionary;

/**
 * Description of Instance
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Instance extends Dictionary
{
    public function getApplication()
    {
        return new Application($this);
    }
    public function getFormName()
    {
        return '';
    }
    public function getPath($fieldNum)
    {
        return '';
    }
}
