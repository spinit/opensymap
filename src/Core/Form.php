<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Core;

use Spinit\Opensymap\Core\Application;
use Spinit\Opensymap\Type\FormAdapterInterface;
use Spinit\Opensymap\Type\TriggerTrait;
use Spinit\Opensymap\Type\TriggerInterface;

use Webmozart\Assert\Assert;

/**
 * Description of Form
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Form implements TriggerInterface
{
    use TriggerTrait;
    
    /**
     *
     * @var Application
     */
    private $app = null;
    
    /**
     *
     * @var FormAdapterInterface
     */
    private $adapter = null;
    
    public function __construct()
    {
        $this->bindExec('save', array($this, 'onSaveExec'));
    }
    /**
     * Impostazione Adapter per l'accesso alla struttura dell'applicazione
     * @param FormAdapterInterface $adapter
     */
    public function setAdapter(FormAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
    /**
     * @return FormAdapterInterface
     */
    public function getAdapter()
    {
        Assert::notNull($this->adapter, 'Form adapter non impostato');
        return $this->adapter;
    }
    
    public function setApplication(Application $app)
    {
        $this->app = $app;
    }
    public function getApplication()
    {
        Assert::notNull($this->app, 'Applicatione non impostata');
        return $this->app;
    }
    
    public function getModel()
    {
        $modelName = $this->getAdapter()->getModelName();
        if (!$modelName) {
            return null;
        }
        return $this->getApplication()->getModel($modelName);
    }
    protected function onSaveExec($event, $param)
    {
        $model = $this->getModel();
        if ($model) {
            $model->trigger('save', $param);
        }
    }
}
