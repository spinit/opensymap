<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Core;

use Spinit\Opensymap\Core\Form;
use Spinit\Opensymap\Core\Instance;
use Spinit\Opensymap\Type\ApplicationAdapterInterface;

use Webmozart\Assert\Assert;
/**
 * Description of Application
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Application
{
    /**
     * @var Instance
     */
    private $instance;
    
    private $formLock;
    
    /**
     * @var ApplicationAdapterInterface
     */
    private $adapter = null;
    
    public function __construct(Instance $instance, $form = '')
    {
        $this->instance = $instance;
        $this->formLock = $form;
    }
    
    /**
     * Impostazione Adapter per l'accesso alla struttura dell'applicazione
     * @param ApplicationAdapterInterface $adapter
     */
    public function setAdapter(ApplicationAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
    /**
     * @return ApplicationAdapterInterface
     */
    public function getAdapter()
    {
        Assert::notNull($this->adapter, 'Application adapter non impostato');
        return $this->adapter;
    }
    public function getInstance()
    {
        return $this->instance;
    }
    public function getFormLock()
    {
        return $this->formLock;
    }
    public function getForm()
    {
        return null;
    }
    public function getModel()
    {
        throw new \Exception();
    }
}
