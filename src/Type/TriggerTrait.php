<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Type;

use Spinit\Opensymap\Error;
use Webmozart\Assert\Assert;
use Spinit\Util\Dictionary;

/**
 * Trigger aggiunge alla classe la possibilità di poter gestire observer su eventi che vengono fatti partire su di esso.
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
trait TriggerTrait
{
    private $observer = array('before' => array(), 'exec' => array(), 'after' => array(), 'end' => array());
   
    /**
     * Segnaposto da implementare per intercettare l'inizio di un trigger
     * @param type $event
     * @param type $param
     */
    protected function triggerBegin($event, $param) {}
    
    /**
     * Segnaposto da implementare per intercettare la fine di un trigger
     * @param type $event
     * @param type $param
     */
    protected function triggerEnd($event, $param) {}
    
    /**
     * Costruttore dell'oggetto Evento
     * @param type $event
     * @param type $param
     * @return \ArrayObject
     */
    private function makeEvent($event, $param)
    {
        $eventStruct = array(
            'name'=>$event,
            'stopPropagation' => function(){
                throw new Error\StopProcessException();
            }
        );
        return new Dictionary($eventStruct);
    }
    
    /**
     * Invocazione di un evento
     * @param type $event
     * @param type $param
     */
    public function trigger($event, $param = array())
    {
        $EventStruct = $this->makeEvent($event, $param);
        
        try {
            $this->triggerBegin($event, $param);
            foreach(array_keys($this->observer) as $when) {
                if (!isset($this->observer[$when][$event])) {
                    continue;
                }
                $EventStruct['when'] = $when;
                foreach($this->observer[$when][$event] as $call) {
                    call_user_func($call[0], $EventStruct, $param, $call[1]);
                }
            }
            $this->triggerEnd($event, $param);
        } catch (Error\StopProcessException $ex) {}
    }
    
    /**
     * Associciazione di un evento ad una callback
     * @param type $when
     * @param type $event
     * @param type $callable
     * @param type $arguments
     */
    public function bind ($when, $event, $callable, $arguments = array())
    {
        Assert::keyExists($this->observer, $when, '('.implode(', ', array_keys($this->observer)).') : '.$when);
        $this->observer[$when][$event][] = array($callable, $arguments);
    }
    /**
     * Scorciatoie
     */
    public function bindBefore($event, $callable, $arguments = array())
    {
        return $this->bind('before', $event, $callable, $arguments);
    }
    public function bindExec($event, $callable, $arguments = array())
    {
        return $this->bind('exec', $event, $callable, $arguments);
    }
    public function bindAfter($event, $callable, $arguments = array())
    {
        return $this->bind('after', $event, $callable, $arguments);
    }
    public function bindEnd($event, $callable, $arguments = array())
    {
        return $this->bind('end', $event, $callable, $arguments);
    }
}
