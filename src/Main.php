<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap;

/**
 * Description of App
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

use Respect\Rest\Router;

use Spinit\UUIDO;
use Spinit\Util;
use Spinit\Opensymap\Controller;
use Spinit\Datasource\DataSource;

use Spinit\Opensymap\Http\Request;
use Spinit\Opensymap\Factory\InstanceFactory;

use Webmozart\Assert\Assert;

class Main
{
    /**
     * Stringa di connessione
     * @var string
     */
    private $cnString;
    /**
     *
     * @var Maker
     */
    private $counter;
    
    /**
     * @var Router
     */
    private $router;
    
    /**
     * Istanza di chiamata
     * @var type
     */
    private $instance;
    
    /**
     * funzione di avanzamento del contatore
     * @var type
     */
    private $nexter;
    
    /**
     * Datasource di riferimento
     * @var DataSource
     */
    private $ds;
    
    public function __construct($connectionString)
    {
        $this->setNexter(null);
        $this->cnString = $connectionString;
        $this->initDSMain($connectionString);
        $this->initRouter();
        $this->addRoute('/__asset/opensymap/**', array(new Controller\AssetController($this), 'any'));
    }
    
    /**
     * Stringa di connessione usata
     * @return string
     */
    public function getConnectionString()
    {
        return $this->cnString;
    }
    
    /**
     * L'istanza viene recuperata tramite factory
     * @param type $domain
     * @param type $path
     * @return type
     */
    public function getInstance($domain, $path)
    {
        $factory = new InstanceFactory($this->getDataSource());
        return $factory->getInstance($domain, $path);
    }
    
    /**
     * Inizializzazione del contatore. Ogni nodo ha un set di numeri dedicato.
     */
    private function initCounter($domain, $uri = '')
    {
        $counter = new UUIDO\Maker($this->getInstance($domain, $uri)['id']);
        $this->counter = $counter;
        // se non è impostata la funzione di next allora viene fornita quella di default
        $this->nexter or $this->setNexterDefault();
        
        $this->counter->setCounter(new UUIDO\Counter($this->nexter));
        return $this->getCounter();
    }
    public function getCounter()
    {
        return $this->counter;
    }
    /**
     * Connessione al Datasource Principale
     */
    private function initDSMain($connectionString)
    {
        $this->ds = new DataSource($connectionString);
    }
    
    /**
     * Mappaggio rotte
     */
    private function initRouter()
    {
        $this->router = new Router();
        $this->router->isAutoDispatched = false;
        return $this->router;
    }
    
    /**
     * Agginge le rotte run-time
     * @param type $name
     * @param type $controller
     */
    public function addRoute($name, $controller, $type = 'any')
    {
        $this->router->$type($name, $controller);
    }
    
    /**
     * E' possibili fornire una funzione di progressione per gli indici del contatore
     * @param type $callback
     */
    public function setNexter($callback)
    {
        $this->nexter = $callback;
    }
    
    /**
     * Se non ne viene fornita nessuna ... allora quella di default si affida al database redis
     * @codeCoverageIgnore
     */
    private function setNexterDefault()
    {
        try {
            Assert::classExists('\\Redis', 'Redis non installato');
            $redis = new \Redis();
            $redis->connect(Util\nvl(getenv('COUNTER_HOST'), 'counter'));
        } catch (\Exception $e) {
            //echo "\n== NEXTER == ".$e->getMessage().' : '.Util\nvl(getenv('COUNTER_HOST'), 'counter')."\n";
            return;
        }
        $this->setNexter(function ($prefix, $node) use ($redis) {
            $val = $redis->hIncrBy($node, $prefix, 1);
            if ($redis->hLen($node) < 10) {
                return $val;
            }
            foreach ($redis->hkeys($node) as $key) {
                if ($key != $prefix) {
                    $redis->hdel($node, $key);
                }
            }
            return $val;
        });
    }
    
    /**
     * L'applicazione viene lanciata
     * @return type
     */
    public function run(Request $request = null)
    {
        $domain = $request ? $request->domain : Util\arrayGet($_SERVER, 'HTTP_HOST');
        $uri =    $request ? $request->uri    : Util\arrayGet($_SERVER, 'REQUEST_URI');
        Assert::notEmpty($domain);
        $this->initCounter($domain, $uri);
        $this->addRoute('/*/**', new Controller\OpensymapController($this, $domain, $uri));
        ob_start();
        $response = $this->router->run($request);
        return ob_get_clean().$response;
    }
    
    public function getDataSource()
    {
        return $this->ds;
    }
}
