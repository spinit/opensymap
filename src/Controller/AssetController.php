<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Controller;

use Respect\Rest\Routable;
use Spinit\Util;

/**
 * Description of AssetController
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class AssetController implements Routable
{
    private $root;
    private $main;
    
    public function __construct($main)
    {
        $this->root = dirname(dirname(__DIR__)).'/asset/';
        $this->main = $main;
    }
    
    public function any($args)
    {
        $filename = $this->root . implode(DIRECTORY_SEPARATOR, $args);
        $path = array();
        $file = false;
        while ($this->root != $filename and strlen($this->root) < strlen($filename)) {
            if (is_file($filename)) {
                $file = pathinfo($filename);
                break;
            }
            $filename = $filename.'.php';
            if (is_file($filename)) {
                $file = pathinfo($filename);
                break;
            }
            $filename = dirname($filename);
            array_unshift($path, array_pop($args));
        }
        return $this->process($file, $path);
    }
    
    private function process($file, $path)
    {
        if (!$file) {
            return 'File not found';
        }
        $headers = array(
            'js' => 'application/javascript',
            'css' => 'text/css',
            'jpg' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
        );
        $file['name'] = $file['dirname'] . DIRECTORY_SEPARATOR . $file['basename'];
        switch (Util\arrayGet($file, 'extension')) {
            case 'php':
                ob_start();
                $result = include($file['name']);
                if (is_string($result) or is_resource($result)) {
                    return $result;
                }
                return ob_get_clean();
            default:
                if (isset($headers[Util\arrayGet($file, 'extension')])) {
                    header('Content-Type: '.$headers[Util\arrayGet($file, 'extension')]);
                }
        }
        return fopen($file['name'], 'r');
    }
}
