<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Controller;

/**
 * Description of MainController
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class OpensymapController
{
    private $main;
    private $domain;
    private $path;
    
    public function __construct($main, $domain, $path)
    {
        $this->main = $main;
        $this->domain = $domain;
        $this->path = $path;
    }
    public function __toString()
    {
        $response = $this->getMain()
            ->getInstance($this->domain, $this->path)
            ->getApplication()
            ->getForm()
            ->trigger($this->getEvent());
        return (string) $response;
    }
    
    public function getEvent()
    {
        return new \stdClass();
    }
    
    public function getMain()
    {
        return $this->main;
    }
}
