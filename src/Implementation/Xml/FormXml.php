<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Implementation\Xml;

use Spinit\Opensymap\Core\Form;
use Spinit\Opensymap\Core\Application;
/**
 * Description of FormXml
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FormXml extends Form
{
    private $indexFile;
    private $index;
    private $struct;
    
    public function __construct(Application $app, $index)
    {
        parent::__construct($app);
        $this->indexFile = is_string($index) ? $index : '';
        $this->index = is_string($index) ? \simplexml_load_file($index) : $index;
        $this->struct = new FormStructXml($this, $this->index);
    }
    public function getStruct()
    {
        return $this->struct;
    }
}
