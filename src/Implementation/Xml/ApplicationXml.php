<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Implementation\Xml;

use Spinit\Opensymap\Core\Application;
use Spinit\Util;
use Webmozart\Assert\Assert;

/**
 * Description of ApplicationXml
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ApplicationXml extends Application
{
    private $rootDir;
    
    public function __construct($instance, $root, $form = '')
    {
        parent::__construct($instance, $form);
        $this->rootDir = $root; 
    }
    public function getForm()
    {
        $formName = Util\biName(Util\nvl($this->getFormLock(), $this->getInstance()->getPath(1)));
        Assert::isArray($formName, "Form non impostata");
        Assert::eq(2, count($formName), "Struttura nome form errata : " . json_encode($formName));
        $formIndex = implode(DIRECTORY_SEPARATOR, array(
            $this->rootDir,
            $formName[0],
            'Form',
            $formName[1],
            Util\nvl($this->getInstance()->getPath(2), 'index').'.xml')
        );
        return new FormXml($this, $formIndex);
    }
}
