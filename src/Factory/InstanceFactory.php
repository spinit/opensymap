<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Factory;

use Spinit\Datasource\DataSource;
use Spinit\Opensymap\Instance;
use Spinit\Opensymap\Lib\Configuration;

/**
 * Description of InstanceFactory
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class InstanceFactory
{
    /**
     *
     * @var DataSource
     */
    private $ds;
    
    public function __construct(DataSource $datasource)
    {
        $this->ds = $datasource;
    }
    
    public function getInstance($domain, $path)
    {
        $conf = new Configuration();
        try {
            $result = $this->ds->load($conf->getQuery('ItemInstance', array('domain'=>$domain, 'path'=>$path)));
            $result->next();
            if ($result->current()) {
                return new Instance\Item($result->current());
            }
            return new Instance\NotFound();
        } catch (\Exception $e) {
            return new Instance\Install($domain, $path);
        }
    }
}
