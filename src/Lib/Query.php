<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Lib;

use Spinit\Datasource\Core\QueryInterface;
use Spinit\Util;

/**
 * Description of Query
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Query implements QueryInterface
{
    private $query;
    
    private $param;
    
    public function __construct($query, $param)
    {
        $this->query = $query;
        $this->param = $param;
    }
    
    public function get($types = '') {
        if (!is_array($this->query)) {
            return $this->query;
        }
        foreach(Util\asArray($types, ',') as $type) {
            if(isset($this->query[$type])) {
                return $this->query[$type];
            }
        }
        throw new \Exception();
    }

    public function getMapper() {
        
    }

    public function getOffset() {
        
    }

    public function getParameters()
    {
        return $this->param;
    }

    public function getSource() {
        
    }

}
