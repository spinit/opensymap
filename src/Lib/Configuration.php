<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Lib;

use Spinit\Util;

/**
 * Cerca il file json o xml nel quale è memorizzata la configurazione dell'oggetto chiamante
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Configuration extends Util\Dictionary
{
    private $result;
    
    private $baseDir;
    
    private $rootDir;
    
    public function __construct($baseDir = '', $rootDir = '../conf')
    {
        $this->baseDir = Util\nvl($baseDir, dirname(__DIR__));
        $this->rootDir = realpath($this->baseDir . DIRECTORY_SEPARATOR . $rootDir);
        $this->result = array();
        $e = new \Exception('Not found');
        $caller = Util\arrayGet($e->getTrace(), 0);
        if (substr($caller['file'], 0, strlen($this->getBase())) != $this->getBase()) {
            throw $e;
        }
        $filename = $this->getRoot() . substr($caller['file'], strlen($this->getBase()), -3);
        try {
            $this->exstractJson($filename.'json');
            $this->exstractXml($filename.'xml');
        } catch (\Exception $ex) {

        }
        parent::__construct($this->result);
    }
    public function getBase()
    {
        return $this->baseDir;
    }
    public function getRoot()
    {
        return $this->rootDir;
    }
    public function getQuery($index, $param = array())
    {
        return new Query($this->get($index), $param);
    }
    private function exstractJson($filename)
    {
        if (!is_file($filename)) {
            return;
        }
        $this->result = json_decode(file_get_contents($filename), 1);
        throw new \Exception();
    }
    
    private function exstractXml($filename)
    {
        if (!is_file($filename)) {
            return;
        }
        $this->result = json_decode(json_encode(simplexml_load_file($filename)), 1);
        throw new \Exception();
    }
}
