<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use PHPUnit_Framework_Assert as Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    private $driver;
    private $session;
    private $pidPhantom;
    
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($webHost)
    {
        $this->urlTest = 'http://'.(getenv('WEB_HOST')?getenv('WEB_HOST'):$webHost).'/';
        echo "Start behat [{$this->urlTest}]\n";
        $this->startPahntom();
        //$this->driver = new \Behat\Mink\Driver\GoutteDriver();
        $this->driver = new \Behat\Mink\Driver\Selenium2Driver();
        $this->session = new \Behat\Mink\Session($this->driver);
        $this->session->start();
    }
    public function __destruct()
    {
        $this->stopPhantom();
    }
    private function startPahntom()
    {
        $command = "phantomjs --webdriver=4444 --load-images=false --max-disk-cache-size=0 > /dev/null 2>&1 & echo $!\n";
        $output = array();
        exec($command, $output);
        $this->pidPhantom = $output[0];
        echo "Start phantomjs [{$this->pidPhantom}]\n";
        
        $start = microtime(true);
        $connected = false;
        
        while (microtime(true) - $start <= (int) 10) {
            if ($this->canConnectTo('localhost', '4444')) {
                $connected = true;
                break;
            }
        }
        
    }
    private function canConnectTo($host, $port)
    {
        // Disable error handler for now
        set_error_handler(function() { return true; });

        // Try to open a connection
        $sp = fsockopen($host, $port);

        // Restore the handler
        restore_error_handler();

        if ($sp === false) {
            return false;
        }

        fclose($sp);

        return true;
    }
    public function stopPhantom()
    {
        $output = array();
        $command = "(sleep 1; kill -15 {$this->pidPhantom}) &\n";
        echo "Stop phantomjs\n";
        exec($command, $output);
    }
    /**
     * @Given I am on the root context
     */
    public function iAmOnTheRootContext()
    {
         $this->session->visit($this->urlTest);
    }

    /**
     * @Then I should see :arg1
     */
    public function iShouldSee($text)
    {
        Assert::assertContains($text, $this->session->getPage()->getContent());
    }
    /**
     * @Given I visit :arg1
     */
    public function iVisit($arg1)
    {
         $this->session->visit($this->urlTest.ltrim($arg1,'/'));
    }

}
