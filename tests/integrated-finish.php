<?php
namespace Spinit\Opensymap\TestInt;

use Spinit\Opensymap\Test\MainTestCase;
use GuzzleHttp\Client;

/**
 * Description of AppTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FinishTest extends MainTestCase
{
    public function testFinish()
    {
        $this->mainFinish();
    }
}
