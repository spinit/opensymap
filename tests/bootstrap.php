<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include(dirname(__DIR__).'/vendor/autoload.php');

ob_start();

// url sulla quale effettuare il test
if (getenv('URL_TEST')) {
    define('URL_TEST', 'http://'.getenv('URL_TEST').'/');
} else {
    if (getenv('urlTest')) {
        define('URL_TEST', 'http://'.getenv('urlTest').'/');
    } else {
        define('URL_TEST', 'http://localhost/');
    }
}
