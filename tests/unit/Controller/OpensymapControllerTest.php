<?php
namespace Spinit\Opensymap\Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use PHPUnit\Framework\TestCase;

/**
 * Description of OpensymapController
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class OpensymapControllerTest extends TestCase
{
    /**
     *
     * @var OpensymapController
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new OpensymapController(new ControllerTest\Main(), 'dominio', 'path');
    }
    public function testDiEsecuzione()
    {
        $this->assertEquals('dominio:path', (string) $this->object);
    }
}

namespace Spinit\Opensymap\Controller\ControllerTest;

class Main {
    private $domain;
    private $path;
    
    public function getInstance($domain, $path)
    {
        $this->domain = $domain;
        $this->path = $path;
        return $this;
    }
    public function getApplication()
    {
        return $this;
    }
    public function getForm()
    {
        return $this;
    }
    public function trigger($event)
    {
        $event->result = $this->domain.':'.$this->path;
        return $event->result;
    }
}
