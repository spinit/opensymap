<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Http;

use PHPUnit\Framework\TestCase;
/**
 * Description of Response
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ResponseTest extends TestCase
{
    public function testResponse()
    {
        $object = new Response('hello world');
        $object->header('X-Sample-Test: ok');
        $this->assertEquals('hello world', (string)$object);
        //$this->assertEquals(array('X-Sample-Test: ok'), headers_list());
    }
    public function testSetGet()
    {
        $object = new Response('hello world');
        $this->assertEquals(null, $object->get('unknown'));
        $object->set('test','ok');
        $this->assertEquals(null, $object->get('unknown'));
        $this->assertEquals('ok', $object->get('test'));
        $this->assertEquals(\json_encode(array('test'=>'ok')), (string)$object);
    }
}
