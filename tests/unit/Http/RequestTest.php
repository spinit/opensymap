<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Http;

use PHPUnit\Framework\TestCase;
/**
 * Description of Request
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class RequestTest extends TestCase
{
    public function testResponse()
    {
        $object = new Request('GET', 'test.tld', '/hello/world','a=b',array('a: b'));
        $this->assertEquals('GET', $object->method);
        $this->assertEquals('test.tld', $object->domain);
        $this->assertEquals('/hello/world', $object->uri);
        $this->assertEquals('a=b', $object->body);
        $this->assertEquals(array('a: b'), $object->headers);
    }
}
