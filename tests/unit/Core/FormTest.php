<?php

/*
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Spinit\Opensymap\Core\Test;

use PHPUnit\Framework\TestCase;
use Spinit\Opensymap\Core\Form;

/**
 * Description of FormStructTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FormTest extends TestCase
{
    /**
     *
     * @var FormStruct
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new Form();
    }
    
    /**
     * @expectedException \Exception
     */
    public function testStructError()
    {
        $this->object->trigger('save');
    }
    public function testStructOk()
    {
        $this->object->setAdapter(new FormTest\FormStruct(''));
        $this->object->trigger('save');
    }
    /**
     * @expectedException \Exception
     */
    public function testApplicationError()
    {
        $this->object->setAdapter(new FormTest\FormStruct('Model:uno'));
        $this->object->trigger('save');
    }
    /**
     * @expectedException \Exception
     */
    public function testApplicationModelError()
    {
        $this->object->setAdapter(new FormTest\FormStruct('Model:uno'));
        $this->object->setApplication(new FormTest\Application('Model:due', $this));
        $this->object->trigger('save');
    }
    public function trigger()
    {
        $this->internalValue = 1;
    }
    public function testApplicationModelOk()
    {
        $this->internalValue = 0;
        
        $this->object->setAdapter(new FormTest\FormStruct('Model:uno'));
        $this->object->setApplication(new FormTest\Application('Model:uno', $this));
        $this->assertEquals(0, $this->internalValue);
        $this->object->trigger('save');
        $this->assertEquals(1, $this->internalValue);
    }
    public function testUnknowEvent()
    {
        $this->object->trigger('evento-sconosciuto-test');
        $this->assertTrue(true);
    }
    public function testAddEvent()
    {
        $testVar = new \Spinit\Util\Dictionary(array('status'=>'init'));
        
        $this->object->bindBefore('test', function() use ($testVar) {
            $testVar['status'] = 'before';
        });
        $this->object->bindAfter('test', function() use ($testVar) {
            $testVar['status'] = 'after';
        });
        $this->assertEquals('init', $testVar['status']);
        $this->object->trigger('test');
        $this->assertEquals('after', $testVar['status']);
    }
    public function testAddEventWhitStop()
    {
        $testVar = new \Spinit\Util\Dictionary(array('status'=>'init'));
        
        $this->object->bindBefore('test', function() use ($testVar) {
            $testVar['status'] = 'before';
        });
        $this->object->bindExec('test', function($event) {
            $event->stopPropagation();
        });
        $this->object->bindEnd('test', function() use ($testVar) {
            $testVar['status'] = 'end';
        });
        $this->assertEquals('init', $testVar['status']);
        $this->object->trigger('test');
        $this->assertEquals('before', $testVar['status']);
    }
}

namespace Spinit\Opensymap\Core\Test\FormTest;

use Spinit\Opensymap\Core\Application as ApplicationCore;
use Spinit\Opensymap\Type\FormAdapterInterface;

class Application extends ApplicationCore
{
    private $modelName;
    
    public function __construct($modelName, $model)
    {
        $this->modelName = $modelName;
        $this->model = $model;
    }
    public function getModel($modelName)
    {
        if ($this->modelName == $modelName) {
            return $this->model;
        }
        return parent::getModel();
    }
}

class FormStruct implements FormAdapterInterface
{
    private $modelName;
    
    public function __construct($model)
    {
        $this->modelName = $model;
    }
    public function getModelName()
    {
        return $this->modelName;
    }
}