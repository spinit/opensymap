<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap;

use PHPUnit\Framework\TestCase;
use Spinit\Opensymap\Http\Request;
use Spinit\Util;

/**
 * Description of AppTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MainTest extends TestCase
{
    /**
     * @var App
     */
    private $object;
    public function setUp()
    {
        $this->object = new Main("sqlite::memory:");
    }
    
    public function testIni()
    {
        $this->object->addRoute('/hello/world/**', array($this, 'routeTest'));
        $this->assertEquals('test ok : ciao', $this->object->run(new Request('GET', 'test.tld', '/hello/world/ciao')));
    }
    public function routeTest($param)
    {
        return 'test ok : '.Util\arrayGet($param, 0);
    }
}
