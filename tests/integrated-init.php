<?php
namespace Spinit\Opensymap\TestInt;

use Spinit\Opensymap\Test\MainTestCase;
use GuzzleHttp\Client;

/**
 * Description of AppTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class InitTest extends MainTestCase
{
    public function testInit()
    {
        // generazione database
        if (!getenv('MYSQL_DATABASE')) {
            $fname = tempnam(sys_get_temp_dir(), 'DB-');
            $strConnection = 'sqlite:'.$fname;
        } else {
            $strConnection = 'mysql:store:'.getenv('MYSQL_DATABASE').':'.
                                            getenv('MYSQL_USER').':'.
                                            getenv('MYSQL_PASSWORD');
        }
        $this->mainInit($strConnection);
    }
};