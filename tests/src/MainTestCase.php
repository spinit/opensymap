<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\Test;

use PHPUnit\Framework\TestCase;
use Spinit\Opensymap\Main;
use Spinit\Opensymap\Http\Request;

/**
 * Description of MainTestCase
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MainTestCase extends TestCase
{
    static $main;
    
    public function mainInit($connectionString)
    {
        self::$main = new Main($connectionString);
    }
    public function mainFinish()
    {
        $cn = explode(':', self::$main->getConnectionString());
        if ($cn[0] == 'sqlite' and $cn[1]) {
            unlink($cn[1]);
        }
    }
    public function getResponse($method, $domain, $uri, $body = '', $headers = array())
    {
        return self::$main->run(new Request($method, $domain, $uri, $body, $headers));
    }
    /**
     * @test
     */
    public function banality()
    {
        $this->assertTrue(true);
    }
}
