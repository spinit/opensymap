<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Opensymap\TestInt\A00;

use Spinit\Opensymap\Test\MainTestCase;

/**
 * Description of AppTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class A00_InitTest extends MainTestCase
{
    public function testInfo()
    {
        $this->assertEquals('Opensymap', $this->getResponse('GET', 'domain.test', '/__asset/opensymap/info/name/'));
    }
    
    public function testInstaller()
    {
        $response = $this->getResponse('GET', 'domain.test', '/test-path/');
        $this->assertThat($response, $this->stringContains('value="domain.test"'));
        $this->assertThat($response, $this->stringContains('value="test-path"'));
    }
}
