<?php
include('../vendor/autoload.php');

$fileConnection = dirname(__DIR__).'/string.connection';
$strConnection = '';
if (is_file($fileConnection)) {
    $strConnection = file_get_contents($fileConnection);
}
if (getenv('MYSQL_DATABASE')) {
    $strConnection = 'mysql:store:'.
        getenv('MYSQL_DATABASE').':'.
        getenv('MYSQL_USER').':'.
        getenv('MYSQL_PASSWORD');
}
$main = new Spinit\Opensymap\Main($strConnection);

echo $main->run();
