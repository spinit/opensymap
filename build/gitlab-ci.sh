#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && [[ ! -e  /.dockerenv ]] && exit 0

#apt-get update -yqq
#apt-get install git zip php-mysql -yqq 

# Enable modules
#docker-php-ext-enable zip
#docker-php-ext-enable pdo pdo_mysql

# Install Xdebug
#pecl install xdebug
#docker-php-ext-enable xdebug

# Install composer
curl -sS https://getcomposer.org/installer | php
# Install project dependencies.
php composer.phar install --no-suggest --prefer-dist

