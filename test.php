<?php
namespace app\Kilin\core\device;

class Printer
{
    private $DB;
    private $maxlen = 46;
    
    public function __construct($DB)
    {
        $this->DB = $DB;
    }
    public function build($id_doc)
    {
        $doc = $this->DB->getFirst("select id, id_str from tbl_inv where id = '{$id_doc}'");
        $printer = $_REQUEST['epsonDevice'];
        if (!$printer) {
            $devices = $this->DB->getAll("select id, nme from str_dev where id_typ = 'PRINTER' and lang = 'EPSON'");
            switch(count($devices)) {
            case 0 :
                return array("exec"=>"PrintForm('pdf.php?id=inv.pdf&id_str={$doc['id_str']}');");
            default:
                return $this->selectDevice($devices, $doc);
            }
        }
        return $this->printDocument($id_doc, $printer);
    }
    
    public function selectDevice($devices, $doc)
    {
        $content = '<div style="border:1px solid silver; background-color:white;">';
        $content .= "<div style='padding:5px;' onclick=\"PrintForm('pdf.php?id=inv.pdf&id_str={$doc['id_str']}');\"> Print PDF</div>";
        $test = '';
        $okTest = 1;
        foreach($devices as $dev) {
            if ($okTest) {
                $test = '<div style="background-color:red; float:right; padding:2px 5px; margin: -2px 2px 2px 2px" onclick="osy.req(this, '."{vars:{print:'epson', epsonDevice:'{$dev['id']}', epsonTest:1}, waitshow:'no', all:1, dataType:'json'}".')">T</div>';
            }
            $content .= "<div style='padding:5px;' onclick=\"osy.req(this, {vars:{print:'epson', epsonDevice:'{$dev['id']}'}, waitshow:'no', all:1, dataType:'json'});\"> {$dev['nme']}</div>{$test}";
        }
        $content .= '</div>';
        return array(
            'exec' => '$(this).before(data.content)',
            'content'=>'<div id="search_content" style="position:absolute;bottom:30px; right:20px; ">'.$content.'</div>'
        );
    }
    
    public function printDocument($id_doc, $printer)
    {
        $data = $this->DB->getFirst("select id, ip_addr, nme from str_dev where id_typ = 'PRINTER' and lang = 'EPSON' and id = '{$printer}'");
        if (!$data['id']) {
            throw new \Exception('Stampante non trovata : '.$printer);
        }
        return array(
        'data'=>$data,
        'url' => "http://{$data['ip_addr']}/cgi-bin/fpmate.cgi",
        'exec'=>"
            var epos = new epson.fiscalPrint();
            epos.onreceive = function (result, tag_names_array, add_info) {
                console.log('receive', arguments);
            };
            epos.send(data.url, data.content);",
        'content' => $_REQUEST['epsonTest'] ? $this->contentNotFiscal() : $this->contentDocument($id_doc));
    }
    
    private function contentNotFiscal()
    {
        return '<printerNonFiscal>' .
					'<beginNonFiscal operator="10" />' .
						'<directIO command="1078" data="1080000"/>' .
					'<endNonFiscal operator="10" />' .
				'</printerNonFiscal>';
    }
    
    private function contentDocument($id_doc)
    {

        $cnt = '<printerFiscalDocument>';
        $cnt .= $this->getIntestazione($id_doc);
        $cnt .= $this->getDettaglio($id_doc);
        $cnt .= $this->getFooter($id_doc);
        $cnt .= '</printerFiscalDocument>';
        return $cnt;
    }
    private function getIntestazione($id_doc)
    {
        $data = $this->DB->getFirst("
select inv.id, inv.num_def, inv.tot_inv, inv.tot_pay, dat4str(inv.dat_cnf) as dat_cnf,
       cli.nme as cli_nme, cli.vat_num as cli_vat, cli.cf as cli_cf,
       cli.add_add as cli_add, cli_cit.dsc as cli_cty, cli.add_zip, cli.add_zon as cli_zon, cli_nat.dsc as cli_nat,
       iss.nme as iss_nme, iss.vat_num as iss_vat, iss.cf as iss_cf,
       iss.add_add as iss_add, iss_cit.dsc as iss_cty, iss.add_zip, cli.add_zon as iss_zon, iss_nat.dsc as iss_nat
from tbl_inv inv
inner join tbl_ana iss on (inv.id_ana_iss = iss.id)
inner join sys_ps_geo iss_cit on (iss.add_cit = iss_cit.cod)
inner join sys_ps_geo iss_nat on (iss.add_nat = iss_nat.cod)
inner join tbl_ana cli on (inv.id_ana_cst = cli.id)
inner join sys_ps_geo cli_cit on (cli.add_cit = cli_cit.cod)
inner join sys_ps_geo cli_nat on (cli.add_nat = cli_nat.cod)
where inv.id = '{$id_doc}'
        ");
        return '<beginFiscalDocument operator="1" documentAmount="'.$data['tot_pay'].'" documentType="freeInvoice" />'.
        $this->printLine($data['iss_nme']).
        $this->printLine(trim($data['iss_vat']) ? $data['iss_vat'] : $data['iss_cf']).
        $this->printLine($data['iss_add']).
        $this->printLine(trim($data['iss_zip'].' '.$data['iss_cty']).($data['iss_nat']?' - '.$data['iss_nat']:'')).
        $this->printLine('').
        $this->printLine('').
        $this->printLine('Cliente / Destinatario', 2).
        $this->printLine($data['cli_nme']).
        $this->printLine(trim($data['cli_vat']) ? $data['cli_vat'] : $data['cli_cf']).
        $this->printLine($data['cli_add']).
        $this->printLine(trim($data['cli_zip'].' '.$data['cli_cty']).($data['cli_nat']?(' - '.$data['cli_nat']):'')).
        $this->printLine('').
        $this->printLine('FATTURA '.$this->lpad($data['num_def'], 10) . '    DEL '.$this->lpad($data['dat_cnf'], 13), 2).
        $this->printLine('Stampato il '.$this->lpad(date('Y/m/d H:i:s'), $this->maxlen-12));
    }
    private function getDettaglio($id_doc)
    {
        $data = $this->DB->getAll("
select det.id_typ, det.dsc, det.qta, ifnull(det.um, 'NM') as um,
       det.prz, det.prz_imp, det.vat_prc, iva.nme as vat_nme,
       det.tot_vat, det.tot_imp, det.tot_row
from tbl_inv_row det
left join tbl_iva iva on (det.id_vat = iva.id)
where det.id_inv = '{$id_doc}'
order by case when det.id_typ = 'det' then 0 else 1 end
");
        $id_typ = 'det';
        $cnt = '';
        foreach($data as $row) {
            if ($id_typ != $row['id_typ']) {
                $id_typ = $row['id_typ'];
                $cnt .= $this->printLine('----------------------------------------------');
            }
            if($id_typ == 'det') {
                $cnt .= $this->makeDettaglioRow($row);
            } else {
                $cnt .= $this->makeDettaglioIva($row);
            }
        }
        $cnt .= $this->printLine('----------------------------------------------');
        return $cnt;
    }
    private function makeDettaglioRow($row)
    {
        $dsc_un = $row['qta'].' x '.$this->prz($row['prz']).'  al'.$row['vat_nme'];
        $dsc_prz = $dsc_un . $this->zpad($row['tot_row'], $this->maxlen - strlen($dsc_un));
        return $this->printLine($row['dsc']).
               $this->printLine($dsc_prz, 1);
    }
    private function makeDettaglioIva($row)
    {
        $cnt = $this->rpad(substr(trim($row['dsc']),0,7), 7);
        $cnt .= $this->zpad($row['tot_imp'], 13);
        $cnt .= $this->zpad($row['tot_vat'], 12);
        $cnt .= $this->zpad($row['tot_row'], 13);
        return $this->printLine('                 IMP         IVA          TOT').
               $this->printLine($cnt);
    }
    private function prz($val)
    {
        return number_format($val, 2, ',', '.');
    }
    private function rpad($str, $len)
    {
        return str_pad(trim($str), $len);
    }
    private function lpad($str, $len)
    {
        return str_pad(rtrim($str), $len, ' ', STR_PAD_LEFT);
    }
    private function zpad($str, $len)
    {
        return $this->lpad($this->prz($str), $len);
    }
    private function getFooter($id_doc)
    {
        return '<endFiscalDocument operator="1" operationType="0" />';
    }
    
    private function printLine($dsc, $font = 1, $type = "")
    {
        $mask = '<printFiscalDocumentLine operator="1" font="'.$font.'" documentLine="%LINE%" />'.PHP_EOL;
        $line = rtrim($dsc);
        if (!strlen($line)) {
            return str_replace('%LINE%', '', $mask);
        }
        $cnt = '';
        while(strlen($line)>0) {
            $cnt .= str_replace('%LINE%', htmlspecialchars(substr($line, 0, $this->maxlen), ENT_QUOTES), $mask);
            $line = substr($line, $this->maxlen);
        }
        return $cnt;
    }
}